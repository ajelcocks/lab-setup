# Prerequisites for Toolchain labs

## Install Node.js

Install the latest version of node from https://nodejs.org/en/

## Install git

Install the latest version of git from https://git-scm.com/downloads

## Install VS Code

If you have not already done so, install VS Code from https://code.visualstudio.com/download

# Setting up VS Code

## Open VS Code

## Configure settings

Click the gear icon in the bottom left of the IDE and select _Settings_ or select _Code > Preferences > Settings_ from the VS Code menu

```javascript
// Place your settings in this file to overwrite the default settings
{
"editor.formatOnPaste": true,
"editor.formatOnSave": true,
"editor.minimap.enabled": false,
"editor.tabSize": 2,
"files.eol": "\n",
"telemetry.enableTelemetry": false,
"telemetry.enableCrashReporter": false,
"workbench.activityBar.visible": true,
```

## VS Code plugins

Click the last (5th) icon (a square) in the toolbar at left.

### ESLint

Type eslint in the search box provided. Select ESLint by Dirk Baeumer

### Prettier

Type Prettier in the search boc provided. Select Prettier - Code formatter by Esben Petersen

### Debugger for Chrome

Type chrome in the search box provided. Select Debugger for Chrome by Microsoft

### EJS language support

Type EJS language in the search box provided. Select EJS language support by DigitalBrainstem

### StatusBar Debugger

Type StatusBar debugger in the search box provided. Select StatusBar Debugger by Fabio Spampinato

### TODO Highlighter

Type TODO highlight in the search box provided. Select TODO Highlight by Wayou Liu

### Gitlens [optional]

Type gitlens in the search box provided. Select GitLens by Eric Amodio
